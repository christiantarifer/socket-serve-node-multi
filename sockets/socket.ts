import { Socket } from 'socket.io';
import socketIO from 'socket.io';
import { UsuariosLista } from '../classes/usuarios-lista';
import { Usuario } from '../classes/usuario';
import { Mapa } from '../classes/mapa';
import { Marcador } from '../classes/marcador';
import { googleMap } from '../routes/router';

export const usuariosConectados = new UsuariosLista();
export const mapa = new Mapa();


// * GOOGLE MAPS

export const marcadorGoogleNuevo = ( cliente: Socket ) => {

    cliente.on( 'marcador-nuevo',  ( marcador ) => {

        googleMap.agregarMarcador( marcador );

        cliente.broadcast.emit( 'marcador-nuevo', marcador );


    });

    cliente.on( 'marcador-borrar', ( id: string ) => {

        googleMap.borrarMarcador( id );

        cliente.broadcast.emit( 'marcador-borrar', id );

    })

    cliente.on( 'marcador-mover', ( marcador ) => {

        googleMap.moverMarcador( marcador );

        cliente.broadcast.emit( 'marcador-mover', marcador );

    })

    

}

// * MAP EVENTS

export const mapaSockets = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('marcador-nuevo', ( marcador: Marcador ) => {

        mapa.agregarMarcador( marcador );

        cliente.broadcast.emit( 'marcador-nuevo', marcador );

    });

    cliente.on('marcador-borrar', ( id: string ) => {

        mapa.borrarMarcador( id );

        cliente.broadcast.emit( 'marcador-borrar', id );

    });

    // marcador-move && broadcast
    cliente.on('marcador-mover', ( marcador: Marcador ) => {

        mapa.moverMarcador( marcador );

        cliente.broadcast.emit( 'marcador-mover', marcador );

    });



}


export const conectarCliente = ( cliente: Socket ) => {

    const usuario = new Usuario( cliente.id );

    usuariosConectados.agregar( usuario );


}

export const desconectar = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('disconnect', () => {

  
        console.log('Client disconnected');

        usuariosConectados.BorrarUsuario( cliente.id );

        io.emit('usuarios-activos', usuariosConectados.getLista() );
        
    })

}

// * LISTEN TO MESSAGES
export const mensaje = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('mensaje', ( payload : {de: string, cuerpo: string } ) => {

        console.log('Message received', payload);

        io.emit('mensaje-nuevo', payload );

    })


}

// * SET UP MESSAGES
export const configurarUsuario = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('configurar-usuario', ( payload: { nombre: string }, callback: Function ) => {

        usuariosConectados.actualizarNombre( cliente.id, payload.nombre );

        io.emit('usuarios-activos', usuariosConectados.getLista() );

        callback({

            ok: true,

            mensaje: `User ${payload.nombre} has been set`

        })

    })

}

// * GET USERS
export const obtenerUsuarios = ( cliente: Socket, io: socketIO.Server ) => {

    cliente.on('obtener-usuarios ', () => {

        io.to( cliente.id ).emit('usuarios-activos', usuariosConectados.getLista() );


    });

}
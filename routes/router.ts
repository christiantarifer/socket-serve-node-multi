import { Router, Request, Response } from 'express';
import Server from '../classes/server';
import { usuariosConectados, mapa } from '../sockets/socket';
import { GraficaData } from '../classes/grafica';
import { EncuestaData } from '../classes/encuesta';
import { MapaGoogle } from '../classes/google-mapa';


const router = Router();

const grafica = new GraficaData();

const encuesta = new EncuestaData();

export const googleMap = new MapaGoogle();

const lugares = [
    {
      id: '1',
      nombre: 'Udemy',
      lat: 37.784679,
      lng: -122.395936
    },
    {
      id: '2',
      nombre: 'Bahía de San Francisco',
      lat: 37.798933,
      lng: -122.377732
    },
    {
      id: '3',
      nombre: 'The Palace Hotel',
      lat: 37.788578,
      lng: -122.401745
    }
  ];

  googleMap.marcadores.push( ...lugares );


// * TEST
/* | ------------------------------------------------------------------------------------------------------- | */

router.get('/mensajes', (req: Request, res: Response) => {

    res.json({
        ok: true,
        mensaje: 'Everything is fine'
    })

})

// * CHARTS - GRAFICA

/* | ------------------------------------------------------------------------------------------------------- | */

router.get('/grafica', (req: Request, res: Response) => {

    res.json( grafica.getDataGrafica() );

})

/* | ------------------------------------------------------------------------------------------------------- | */

router.post('/grafica', (req: Request, res: Response) => {

    const mes: string = req.body.mes;

    const unidades: number = Number( req.body.unidades );

    grafica.incrementarValor( mes, unidades );

    const server = Server.instance;

    server.io.emit( 'cambio-grafica', grafica.getDataGrafica() );

    res.json( grafica.getDataGrafica() );

})

// * CHARTS - ENCUESTA

/* | ------------------------------------------------------------------------------------------------------- | */

router.get('/bands-labels', (req: Request, res: Response) => {

    res.json( encuesta.getDataBands() );

})

/* | ------------------------------------------------------------------------------------------------------- | */

router.get('/bands-popularity', (req: Request, res: Response) => {

    res.json( encuesta.getDataEncuesta() );

})

/* | ------------------------------------------------------------------------------------------------------- | */

router.post('/bands-popularity', ( req: Request, res: Response ) => {

    const band: string = req.body.band;

    const point: number = Number( req.body.point );

    encuesta.incrementBandPopularity( band, point );

    const server = Server.instance;

    server.io.emit( 'cambio-encuesta', encuesta.getDataEncuesta() );

    res.json( encuesta.getDataEncuesta() );

});

// * MAPBOX - MAPS

/* | ------------------------------------------------------------------------------------------------------- | */

router.get('/mapa', ( req: Request, res: Response ) => {

    res.json( mapa.getMarcadores() );

});

/* | ------------------------------------------------------------------------------------------------------- | */

// * GOOGLE - MAPS

router.get('/google-maps', ( req: Request, res: Response ) => {

    res.json( googleMap.getMarcadores() );

});

/* | ------------------------------------------------------------------------------------------------------- | */

/* | ------------------------------------------------------------------------------------------------------- | */

// * MESSAGES

/* | ------------------------------------------------------------------------------------------------------- | */

router.post('/mensajes', (req: Request, res: Response) => {

    

    const cuerpo = req.body.cuerpo;

    const de    = req.body.de;

    const payload = { de, cuerpo }

    const server = Server.instance;

    server.io.emit('mensaje-nuevo', payload);

    res.json({
        ok: true,
        cuerpo,
        de
    })

})

/* | ------------------------------------------------------------------------------------------------------- | */

router.post('/mensajes/:id', (req: Request, res: Response) => {

    const cuerpo = req.body.cuerpo;

    const de    = req.body.de;

    const id    = req.params.id;

    const payload = {

        de,
        cuerpo

    }

    const server = Server.instance;

    server.io.in( id ).emit('mensaje-privado', payload);

    res.json({
        ok: true,
        cuerpo,
        de,
        id
    })

})

/* | ------------------------------------------------------------------------------------------------------- | */

// * GET ALL USERS'S ID

router.get('/usuarios', ( req: Request, res: Response) => {

    const server = Server.instance;

    server.io.clients( (err: any , clientes: string[]) => {

        if ( err ) {

            res.json({

                ok: false,
                err

            })

        }

        res.json({

            ok: true,
            clientes

        })

    })

})


/* | ------------------------------------------------------------------------------------------------------- | */

// * GET ALL USERS AND NAMES

router.get('/usuarios/detalle', ( req: Request, res: Response) => {

    const clientes = usuariosConectados.getLista();

        res.json({

            ok: true,
            clientes

        })


})

/* | ------------------------------------------------------------------------------------------------------- | */

export default router;
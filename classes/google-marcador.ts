export class GoogleMarcador{

    constructor(
        public id: string,
        public nombre: string,
        public lat: number,
        public lng: number
    ) {}

}
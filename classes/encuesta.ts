export class EncuestaData {


    private bands: string[] = ['first to eleven', 'against the current', 'halocene', 'lauren Babic'];

    private points: number[] = [60, 80, 40, 35];

    constructor() {}

    getDataBands() {

        return this.bands;

    }

    getDataEncuesta() {
        
        return [
            { data: this.points, label: 'Popularity' }
        ]

    }

    incrementBandPopularity( band: string, point: number) {

        band = band.toLowerCase().trim();

        console.log(band);
        console.log(point);
        for (const i in this.bands) {
            
            if ( this.bands[i] === band ) {
                console.log(band);
                this.points[i] += point;

            }

        }

        return this.getDataEncuesta();

    }

}
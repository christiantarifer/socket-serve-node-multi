import { GoogleMarcador } from './google-marcador';

export class MapaGoogle{

    public marcadores: GoogleMarcador[] = [];

    getMarcadores() {

        return this.marcadores;

    }

    agregarMarcador( marcador: GoogleMarcador ) {

        this.marcadores.push( marcador )

    }

    borrarMarcador( id: string ){

        this.marcadores.filter( mark => mark.id !== id );

        return this.marcadores;

    }

    moverMarcador( marcador: GoogleMarcador ){

        for (const i in this.marcadores) {
            if ( this.marcadores[i].id === marcador.id ){
                this.marcadores[i].lat = marcador.lat;
                this.marcadores[i].lng = marcador.lng;
                break;

            }
                
        }

    }

}